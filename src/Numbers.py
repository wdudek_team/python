count=0

for number in range(0,100000):
    # number is a form of dcba
    a=number//10000
    b=(number - 10000*a)//1000
    c=(number - 10000*a -1000*b)//100
    d=(number - 10000*a -1000*b - 100* c)
    e=(number - 10000 * a - 1000 * b - 100 * c - 10 * d)
    numberWithOneAtLeft=e + 10 * d + 100 * c + 1000 * b + 10000 * a + 100000
    numberWithOneAtRight=1 + 10 * e + 100 * d + 1000 * c + 10000 * b + 100000 * a
    #print("number: %d, numberWithOneAtLeft: %d, numberWithOneAtLeft*3: %d, numberWithOneAtRight: %d"%
    #      (number, numberWithOneAtLeft,numberWithOneAtLeft * 3, numberWithOneAtRight))

    if (numberWithOneAtLeft*3==numberWithOneAtRight):
        print("Number %d is such a number"%number)
        count+=1
        found=number
    else:
        pass
        #print("The difference: %f",numberWithOneAtLeft*3 - numberWithOneAtRight)

print("Number of such numbers: %d, last one: %d"%(count,found))

print("============================================================")
numbers=set()
print("Another scenario")
for number in range(10000,100000):
    a=number%10000+10000
    b=number//10*10+1
    #print("a=%d, b=%d"%(a,b))
    if (a*3-b==0):
        numbers.add(number)
        #print("Number found: %d"%number)

if (len(numbers)>0):
    print("The numbers are: %s"%str(numbers))
else:
    print("No such numbers")