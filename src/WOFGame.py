import requests
import random
import string
import time

#length=24
wheelRewards=[1500,350,500,0,159,200,0,250,500,-1,400,2500,250,0,400,200,100,-1,150,250,300,0,200,-1]
inThePot={'human':0,'computer':0}
visibleLetters=[]

def getWord(theme="music",minChars=7):
    url = 'http://api.datamuse.com/words/?rel_trg={}&max=500'.format(theme)

    resp = requests.get(url=url)
    data = resp.json()
    # print(data)

    # Sample data for testing
    # data=[{'word': 'videos', 'score': 1345}, {'word': 'conservatory', 'score': 1314}, {'word': 'video', 'score': 1306}, {'word': 'genres', 'score': 1300}, {'word': 'lyrics', 'score': 1276}, {'word': 'mtv', 'score': 1271}, {'word': 'composer', 'score': 1257}, {'word': 'ensemble', 'score': 1249}, {'word': 'youtube', 'score': 1248}, {'word': 'jazz', 'score': 1242}, {'word': 'folk', 'score': 1239}, {'word': 'piano', 'score': 1234}, {'word': 'violin', 'score': 1233}, {'word': 'conductor', 'score': 1230}, {'word': 'composed', 'score': 1229}, {'word': 'composition', 'score': 1228}, {'word': 'awards', 'score': 1226}, {'word': 'orchestra', 'score': 1219}, {'word': 'festival', 'score': 1218}, {'word': 'symphony', 'score': 1213}, {'word': 'grammy', 'score': 1210}, {'word': 'musical', 'score': 1209}, {'word': 'pop', 'score': 1205}, {'word': 'genre', 'score': 1204}, {'word': 'soundtrack', 'score': 1203}, {'word': 'hop', 'score': 1200}, {'word': 'duo', 'score': 1198}, {'word': 'choir', 'score': 1195}, {'word': 'critics', 'score': 1194}, {'word': 'dance', 'score': 1191}, {'word': 'instruments', 'score': 1189}, {'word': 'label', 'score': 1186}, {'word': 'bands', 'score': 1185}, {'word': 'classical', 'score': 1184}, {'word': 'critic', 'score': 1180}, {'word': 'hip', 'score': 1176}, {'word': 'premiered', 'score': 1174}, {'word': 'vocal', 'score': 1172}, {'word': 'charts', 'score': 1170}, {'word': 'songs', 'score': 1168}, {'word': 'punk', 'score': 1166}, {'word': 'concert', 'score': 1164}, {'word': 'singing', 'score': 1163}, {'word': 'artists', 'score': 1160}, {'word': 'styles', 'score': 1159}, {'word': 'blues', 'score': 1157}, {'word': 'influences', 'score': 1156}, {'word': 'sounds', 'score': 1154}, {'word': 'song', 'score': 1145}, {'word': 'album', 'score': 1140}, {'word': 'released', 'score': 1117}]

    selectedWords=[r['word'] for r in data if len(r['word'])>=minChars]
    #print(selectedWords)
    return random.choice(selectedWords)

def chooseCategoryAndWord():
    categoryAndCount = input("Get category (min 3 chars) and min word length (default: music,7):")

    categoryAndCountArr = categoryAndCount.split(',')
    category = str(categoryAndCountArr[0])
    if len(category.strip()) < 3:
        category = 'music'

    try:
        minCharacters = int(categoryAndCountArr[1])
    except (ValueError, IndexError):
        minCharacters = 7

    print("Category: %s, Min characters: %d" % (category, minCharacters))
    rewardsToDisplay=list(map(lambda x: str(x) if x > 0 else "BANKRUT" if x<0 else "STOP", wheelRewards))
    print("Wheel of rewards is the following: %s"%str(rewardsToDisplay))
    return (category,getWord(category, minCharacters))

#---------------------------------------------------------------------------

def shuffle():
    return random.choice(wheelRewards)

def solveIn(percentHit):
    return random.randint(0,100)<percentHit

def giveLetter():
    letter=str(input("Give a letter:"))
    if len(letter)!=1 or not letter.isalpha():
        return None
    else:
        return letter

def showWord(word,visibleLetters):
    return ''.join([('_',ch)[ch in visibleLetters] for ch in list(word)])

def getNumberOfHits(word,visibleLetters,letter):
    if letter in visibleLetters:
        print("Given before")
        return 0
    else:
        return word.count(letter)

def processReward(inThePotBefore, price,hits):
    rewarded = price * hits
    inThePotAfter=inThePotBefore+rewarded
    if (inThePotAfter>0):
        print("You win: %d"%rewarded)
    return inThePotAfter

def printTurn(turnNr, isHuman):
    print("===================================================")
    print("Turn %d for %s"%(turnNr,'you' if isHuman else 'computer'))

def printStats(inThePot):
    print("In the pot. You: %d, Computer: %d "%(inThePot['human'],inThePot['computer']))

def printWord(category,word,visibleLetters):
    print("CATEGORY: %s, WORD: %s" % (category, showWord(word, visibleLetters)))

def turn(category, word,visibleLetters,inThePot,isHuman,turnNr):
    printTurn(turnNr, isHuman)
    printWord(category, word, visibleLetters)
    price=shuffle()
    if (price>0):
        print("Price for each letter: %d PLN"%price)
    elif price<0:
        print("You are broke!")
        inThePot[('computer','human')[isHuman]] = 0
        printStats(inThePot)
        return not isHuman
    else:
        print("Loss of turn")
        printStats(inThePot)
        return not isHuman
        
    if isHuman:
        letter=giveLetter()
        while not letter:
            print("Not a letter. Try again")
            letter = giveLetter()
    else:
        time.sleep(2)
        letter=random.choice(string.ascii_lowercase)

    print("Letter to check %s"%letter)
    hits=getNumberOfHits(word, visibleLetters, letter)
    print("HITS: %s"%hits)
    inThePot[('computer','human')[isHuman]] = processReward(inThePot[('computer','human')[isHuman]], price, hits)
    visibleLetters.append(letter)
    printWord(category, word, visibleLetters)
    printStats(inThePot)

    if hits==0:
        return not isHuman
    else:
        if isHuman:
            wordGuess=input("------Can you guess?:--------")
            if wordGuess==word:
                print("You win!")
                exit()
            else:
                print("Not a match")
        else:
            print("Trying to guess")
            if solveIn(20): #1/5 chance to guess
                time.sleep(2)
                print("Word is %s"%word)
                print("Computer win!")
                exit()
            else:
                print("Not a match")

        return isHuman

turnNr=0
isHuman=True
categoryAndWord=chooseCategoryAndWord()
category=categoryAndWord[0]
word=categoryAndWord[1]
print("CATEGORY: %s, WORD: %s"%(category,showWord(word, visibleLetters)))

while True:
    turnNr+=1
    isHuman=turn(category, word, visibleLetters, inThePot,isHuman, turnNr)
