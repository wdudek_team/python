import random

rewards={0: 0, 1: 0, 2: 0, 3: 25, 4: 190, 5: 5000, 6: 6000000}

def reward(times,userNumbers):
    sum=0
    for i in range(1,times+1):
        sum+=rewards[shuffle(userNumbers)]
    return sum

def shuffle(userNumbers):
    rewardNumbers=set()
    #print(userNumbers)
    for i in range(1,7):
        rewardNumbers.add(random.randint(1, 49))

    #print(rewardNumbers)
    match=rewardNumbers.intersection(userNumbers)
    if (len(match)>1):
        print("You win. Hits: %d"%len(match))

    else:
        pass
        #print("You loose")
    return len(match)

if __name__ == '__main__':
    numbers = input('Enter 6 numbers from 1-49 separated by comma: ')
    userIntNumbers=numbers.split(",")

    try:
        userIntNumbers=[int(i) for i in userIntNumbers]
    except ValueError:
        print("You need to put integer values")
        quit()

    userIntNumbers=[i for i in userIntNumbers if 1<=i<=49]
    if (len(userIntNumbers)!=6):
        print("You need to put 6 values in the range 1..49")
        quit()

    userNumbers=set(userIntNumbers)

    howMany=20000
    rewarded=reward(howMany,userNumbers)
    print("Outcome: %d, Income: %d, Loss in PLN: %d"%(2*howMany,rewarded,2*howMany-rewarded))

