from unittest import TestCase
from unittest.mock import patch
from Lotto import reward, shuffle

class TestLottoMethods(TestCase):
    @patch('Lotto.shuffle', return_value=3)
    def test_reward(self,shuffle):
        self.assertEqual(reward(2,1), 50)

    def test_shuffle(self):
        for i in range(1,1000):
            self.assertTrue(0<=shuffle([1])<=6)

if __name__ == '__main__':
    unittest.main()